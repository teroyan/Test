// pages/demo02/demo02.js
Page({
  /**
   * 页面的初始数据
   */
  data: {
    num:0
  },
  //inout事件的执行逻辑
  handleInput(e){
    // console.log("触发输入事件");
    this.setData({
      num:e.detail.value
    })
  },

  // button触发事件
    // 可通过在ml中用data-自定义属性，传递参数
    // 并在事件逻辑中通过"e.currentTarget.dataset.属性名" 来获取属性值
  handleTap(e){
    console.log(e.currentTarget.dataset.operation);
    const operation=e.currentTarget.dataset.operation;
    this.setData({
      num:this.data.num+operation
    })
  }
})